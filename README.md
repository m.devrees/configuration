# Configuration
This repo is to keep some of my configuration at a place where it's easily reachable (I can fetch it using curl/wget).
## Appending to files
Append configs to your files
```bash
curl https://raw.githubusercontent.com/mdevrees/configuration/master/.bashrc >> ~/.bashrc
curl https://raw.githubusercontent.com/mdevrees/configuration/master/.screenrc >> ~/.screenrc
curl https://raw.githubusercontent.com/mdevrees/configuration/master/.gitconfig >> ~/.gitconfig
```

or via gitlab
```bash
curl https://gitlab.com/m.devrees/configuration/-/raw/master/.bashrc >> ~/.bashrc
curl https://gitlab.com/m.devrees/configuration/-/raw/master/.vimrc >> ~/.vimrc
curl https://gitlab.com/m.devrees/configuration/-/raw/master/.screenrc >> ~/.screenrc
curl https://gitlab.com/m.devrees/configuration/-/raw/master/.gitconfig >> ~/.gitconfig
curl https://gitlab.com/m.devrees/configuration/-/raw/master/.Xresources >> ~/.Xresources
curl https://gitlab.com/m.devrees/configuration/-/raw/master/zsh-aliases/custom-aliases.zsh >> $ZSH_CUSTOM/custom-aliases.zsh
curl https://gitlab.com/m.devrees/configuration/-/raw/master/zsh-aliases/virtualenvwrapper.zsh >> $ZSH_CUSTOM/virtualenvwrapper.zsh
curl https://gitlab.com/m.devrees/configuration/-/raw/master/zsh-aliases/custom.zsh >> $ZSH_CUSTOM/custom.zsh

```

ZSH Plugins and themes
```bash
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/lukechilds/zsh-nvm.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-nvm
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
```

## Fonts
I use a few fonts, some are nerd fonts and some are useful terminal fonts. Copy the `fonts` contents to `~/.local/share/fonts`.  
Next, run
```bash
$ cp -r fonts/* ~/.local/share/fonts/
$ fc-cache -fv
$ fc-list
```
to regenerate fonts and check the list of it is complete.
I prefer the Adobe Source Code Pro font in my terminal.

## Terminals
### gnome-terminal
gnome-terminal is the default terminal on most debian systems (Ubuntu, Linux Mint)
#### gnome-terminal color scheme
I'm a fan of the Nord color scheme, as it is not too flashy and colors don't jump all over the place. Check `https://github.com/arcticicestudio/nord-gnome-terminal` for installation instructions.
```bash
$ git clone https://github.com/arcticicestudio/nord-gnome-terminal.git
$ cd nord-gnome-terminal/src
$ ./nord.sh Nord
```
if the above command outputs Nord is not a valid profile, create a profile named Nord first in the gnome-terminal settings under Edit > Preferences

### Alacritty
Alacritty is a lightweight terminal emulator which is very easy to install. There are detailed instructions on the alacritty github page on how to build this terminal. There's a config file in my [dotconfig repository](https://gitlab.com/m.devrees/dotconfig-files).

## vimrc
There is the 'ultimate' vim configuration repository over on Github: https://github.com/amix/vimrc which sets some awesome .vimrc configurations automatically.
To install, simply run
```bash
git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
sh ~/.vim_runtime/install_awesome_vimrc.sh
```
You can then add some custom configurations to `~/.vim_runtime/my_configs.vim`. For example, if you want line numbers to always show:
```bash
echo "set numbers" >> ~/.vim_runtime/my_configs.vim
```

## nanorc
Use `https://github.com/scopatz/nanorc` configs for nano configuration.  
Automatic installer:  
```bash
curl https://raw.githubusercontent.com/scopatz/nanorc/master/install.sh | sh
echo "set tabsize 4\nset tabstospaces" >> ~/.nanorc
```
Or to do so manually, append:
```bash
set tabsize 4
set tabstospaces
```
to your `~/.nanorc` file to set this as default

## Icons
There's a nice icon set that you can download: https://snwh.org/moka  
Installation can be done via repository and switched to via `lxappearance`

Copy the icons in the `icons/` folder to `/usr/share/icons/`. Icons can be changed by editing 
`/home/mycha/.config/gtk-3.0/settings.ini` and if you used lxappearance, edit `~/.gtkrc-2.0` or create a `~/.gtkrc-2.0.mine` file with the changes

## Theme
A theme that looks a bit like Nord is the Arc theme: https://linuxhint.com/install_arc_gtk_theme_ubuntu/
